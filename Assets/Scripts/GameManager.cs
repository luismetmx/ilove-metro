﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This Game Manager uses the singleton pattern to handle the game state changes on the game
/// </summary>

// Game States

public enum GameState
{ SplashScreen, MainMenu, Levels, Tutorial, Ranking, GamePlay, Logros, GameOver, Pausa, Victoria }

public delegate void OnStateChangeHandler();

public class GameManagerBH : MonoBehaviour
{
    private static GameManagerBH instance = null;
    public event OnStateChangeHandler OnStateChange;
    public GameState gameState { get; private set; }

    protected GameManagerBH()
    {

    }

    public static GameManagerBH Instance
    {
        get
        {
            if (GameManagerBH.instance == null)
            {
                GameManagerBH.instance = new GameManagerBH();
                DontDestroyOnLoad(GameManagerBH.instance);
            }
            return GameManagerBH.instance;
        }

    }

    public void SetGameState(GameState state)
    {
        this.gameState = state;
        OnStateChange();
    }

    public void OnApplicationQuit()
    {
        GameManagerBH.instance = null;
    }

}


