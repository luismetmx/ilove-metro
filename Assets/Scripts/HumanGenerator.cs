﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System;
using UnityEngine;

/// <summary>
/// This Script generates Humans on the scene
/// </summary>

public class HumanGenerator : MonoBehaviour
{
    public GameObject[] humans;
    public GameObject[] spawnPoints;
    float time;
    float timeWaiting;
	public TextAsset xmlLevelConfig;
	int maxHumanLevel;
	int humanCounter;
    // Use this for initialization

    void Start ()
    {
        time = 0;
        timeWaiting = 2.5f;
		LoadData ();

        if(GamePlayBH.startGame)
        {
            CrearHumano();
			humanCounter = 1;
        }
       
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(GamePlayBH.startGame)
        {
            time += Time.deltaTime;

            if (time > timeWaiting)
            {
                time = 0.0f;
                CrearHumano();
            }
        }

	}

    void CrearHumano()
    {
		humanCounter++;
		if (humanCounter < maxHumanLevel) 
		{
			Instantiate (humans [UnityEngine.Random.Range (0, humans.Length)], spawnPoints [UnityEngine.Random.Range (0, spawnPoints.Length)].transform.position, Quaternion.identity);
		}
    }

	void LoadData()
	{
		List<LevelConfig> listaLevels = new List<LevelConfig> ();

		if (xmlLevelConfig != null) 
		{

			XDocument xDoc = XDocument.Parse(xmlLevelConfig.text);

			listaLevels = (from item in xDoc.Descendants ("level")
				where item.Element ("id").Value.ToString() == LevelsBH.level.ToString()
				select new LevelConfig () 
				{
					id = int.Parse(item.Element ("id").Value),
					humans = int.Parse(item.Element("humans").Value)
				}).ToList ();
			
			maxHumanLevel = listaLevels.ElementAt (0).humans;
		} 

		else 
		{
			Debug.Log ("No puedo acceder al archivo");
		}
	}
}
