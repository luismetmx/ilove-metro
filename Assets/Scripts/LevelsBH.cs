﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// This is a Script to control the Levels Scene (UI interactions and logic to unlock levels)
/// </summary>

public class LevelsBH : MonoBehaviour
{
    GameManagerBH gameManager;
	public static int level;
	SaveLoadData datos;
	PlayerData playerInfo;
	public GameObject [] lockLevels;
	public GameObject [] unlockLevels;

    void Awake()
    {
        gameManager = GameManagerBH.Instance;
    }

    void Start()
    {
		gameManager.OnStateChange += HandleOnStateChange;
		gameManager.SetGameState (GameState.Levels);
		datos = new SaveLoadData ();
		playerInfo = datos.Load ();
		CheckUnlockLeves ();
    }

    public void OnLevel(int levelSelect)
    {
		level = levelSelect;
		SceneManager.LoadScene ("GamePlay");
    }

    public void HandleOnStateChange()
    {
        Debug.Log("Estoy en el estado: " + gameManager.gameState);
    }

	void CheckUnlockLeves()
	{
		for (int i = 0; i < playerInfo.levelsStatus.Length; i++) 
		{
			if (playerInfo.levelsStatus [i] == 1) 
			{
				lockLevels [i].SetActive (false);
				unlockLevels [i].SetActive (true);
			} 
			else 
			{
				lockLevels [i].SetActive (true);
				unlockLevels [i].SetActive (false);
			}
		}
	}
}
