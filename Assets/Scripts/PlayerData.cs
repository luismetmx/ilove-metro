﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This is a model to handle the progress of the player (lock & unlock levels)
/// Include other info in the future (like facebook id, image, etc.)
/// </summary>

[System.Serializable]

public class PlayerData  
{
	public int[] levelsStatus = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	public PlayerData()
	{
	}
}
