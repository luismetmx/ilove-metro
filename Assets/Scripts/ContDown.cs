﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This Script Handle the ContDown to start the game
/// </summary>

public class ContDown : MonoBehaviour
{
    public static bool starGame;
    Animator anim;
	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        starGame = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (MetroBH.metroArrive)
        {
            anim.SetBool("isContDown", true);
            float lengtOfAnim = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            Debug.Log("length of animation: " + lengtOfAnim);
            if (lengtOfAnim > 5.0f)
            {
                starGame = true;
                Destroy(gameObject);
            }
        }
    }
}
