﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// This script handle the behavior of TutorialController scene
/// </summary>

public class TutorialController : MonoBehaviour 
{
	GameManagerBH gameManager;


	void Awake()
	{
		gameManager = GameManagerBH.Instance;
	}

	// Use this for initialization
	void Start () 
	{
		gameManager.OnStateChange += HandleOnStateChange;
		gameManager.SetGameState (GameState.Tutorial);
	}

	public void HandleOnStateChange()
	{
		Debug.Log("Estoy en el estado: " + gameManager.gameState);
	}

	public void OnTutorialSkip()
	{
		SceneManager.LoadScene ("MainMenu");
	}
}
