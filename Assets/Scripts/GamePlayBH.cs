﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine.SceneManagement;

/// <summary>
/// This Script handle the game logic, points, lives and pause and game over state
/// Use PlayerData class to save the progress on the game (the progress on the game is on a encripted file)
/// </summary>


public class GamePlayBH : MonoBehaviour
{
    public static int lives;
    public GameObject[] liveImages;
    public GameObject[] noLiveImages;
	public GameObject[] fondoLevel;
    public static int points;
    public GameObject CountDown;
	public static bool startGame;
	public Text pointsText;
	public GameObject gameOverCanvas;
	public GameObject victoriaCanvas;
	public GameObject pausaCanvas;
    Animator anim;
	GameManagerBH gameManager;
	SaveLoadData datos;
	PlayerData playerInfo;

	void Awake()
	{
		gameManager = GameManagerBH.Instance;
	}
		
	// Use this for initialization
	void Start ()
    {
		gameManager.OnStateChange += HandleOnStateChange;
		gameManager.SetGameState (GameState.GamePlay);
        startGame = false;
        lives = 3;
        points = 0;
		pointsText.text = points.ToString();
		anim = CountDown.GetComponent<Animator>();
		Instantiate (fondoLevel [LevelsBH.level], Vector3.zero, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update ()
    {
		pointsText.text = points.ToString();

        if (MetroBH.metroArrive)
        {
            StartCoroutine("StartCountDown");
			//MetroBH.metroArrive = false;
        }

		if (lives == 2) 
		{
			liveImages [2].SetActive (false);
			noLiveImages [2].SetActive (true);
		}

		if (lives == 1) 
		{
			liveImages [1].SetActive (false);
			noLiveImages [1].SetActive (true);
		}

		if (lives == 0) 
		{
			liveImages [0].SetActive (false);
			noLiveImages [0].SetActive (true);
		}

		if (lives <= 0) 
		{
			lives = 0;
			gameManager.SetGameState (GameState.GameOver);
		}

		if (ZombieGenerator.hordaComplete) 
		{
			gameManager.SetGameState (GameState.Victoria);
		}
    }

    IEnumerator StartCountDown()
    {
		if (startGame == false) 
		{
			anim.SetBool ("isContDown", true);
			yield return new WaitForSeconds (5.0f);
			CountDown.SetActive (false);
			startGame = true;
		} 
		else 
		{
			yield break;
		}
        
    }

	public void HandleOnStateChange()
	{
		Debug.Log("Estoy en el estado: " + gameManager.gameState);

		if (gameManager.gameState == GameState.GamePlay) 
		{
			Time.timeScale = 1;
			gameOverCanvas.SetActive (false);
			victoriaCanvas.SetActive (false);
			pausaCanvas.SetActive (false);
		}

		if (gameManager.gameState == GameState.GameOver) 
		{
			Time.timeScale = 0;
			gameOverCanvas.SetActive (true);
			victoriaCanvas.SetActive (false);
			pausaCanvas.SetActive (false);
		}

		if (gameManager.gameState == GameState.Pausa) 
		{
			Time.timeScale = 0;
			gameOverCanvas.SetActive (false);
			victoriaCanvas.SetActive (false);
			pausaCanvas.SetActive (true);
		}

		if (gameManager.gameState == GameState.Victoria) 
		{
			//Time.timeScale = 0;
			gameOverCanvas.SetActive (false);
			victoriaCanvas.SetActive (true);
			pausaCanvas.SetActive (false);
			datos = new SaveLoadData ();
			playerInfo = datos.Load ();

			for (int i = 0; i < playerInfo.levelsStatus.Length; i++) 
			{
				if (i == LevelsBH.level) 
				{
					playerInfo.levelsStatus [i] = 1;
				}
			}

			datos.Save (playerInfo.levelsStatus);
		}
	}
    
	public void OnMenu()
	{
		SceneManager.LoadScene ("Levels");
	}

	public void OnRetry()
	{
		SceneManager.LoadScene ("GamePlay");
	}

	public void OnShare()
	{
		//Agregar codigo para compartir en Facebook
	}

	public void OnNextLevel()
	{
		LevelsBH.level = LevelsBH.level + 1;
		SceneManager.LoadScene ("GamePlay");
	}

	public void OnResume()
	{
		gameManager.SetGameState (GameState.GamePlay);
	}

	public void OnPause()
	{
		gameManager.SetGameState (GameState.Pausa);
	}
}
