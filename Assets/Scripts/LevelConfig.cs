﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This is a Model for the Configuration of each level
/// </summary>

public class LevelConfig 
{
	public int id{ get; set; }
	public int humans{ get; set; }
	public int enemies{ get; set; }
	public float velocidad{ get; set; }
}
