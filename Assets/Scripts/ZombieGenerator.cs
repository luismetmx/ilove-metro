﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System;

/// <summary>
/// This script creates intances of Zombie prefabs on the scene and uses de Levels.xml file to set the number max of 
/// enemies on the scene
/// </summary>

public class ZombieGenerator : MonoBehaviour
{
    public GameObject[] zombies;
    public GameObject[] spawnPoints;
    float time;
    float timeWaiting;
	public TextAsset xmlLevelConfig;
	int maxZombieLevel;
	int zombieCounter;
	public static bool hordaComplete;
    // Use this for initialization

    void Start()
    {
        time = 0;
        timeWaiting = 3.5f;
		hordaComplete = false;
		LoadData ();

        if (GamePlayBH.startGame)
        {
            CrearZombie();
			zombieCounter = 1;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(GamePlayBH.startGame)
        {
            time += Time.deltaTime;

            if (time > timeWaiting)
            {
                time = 0.0f;
                CrearZombie();
            }
        }

    }

    void CrearZombie()
    {
		zombieCounter++;
		if (zombieCounter < maxZombieLevel) 
		{
			Instantiate (zombies [UnityEngine.Random.Range (0, zombies.Length)], spawnPoints [UnityEngine.Random.Range (0, spawnPoints.Length)].transform.position, Quaternion.identity);
		} 
		else 
		{
			hordaComplete = true;
		}
    }

	void LoadData()
	{
		List<LevelConfig> listaLevels = new List<LevelConfig> ();

		if (xmlLevelConfig != null) 
		{

			XDocument xDoc = XDocument.Parse(xmlLevelConfig.text);

			listaLevels = (from item in xDoc.Descendants ("level")
				where item.Element ("id").Value.ToString() == LevelsBH.level.ToString()
				select new LevelConfig () 
				{
					id = int.Parse(item.Element ("id").Value),
					enemies = int.Parse(item.Element("enemies").Value)
				}).ToList ();

			maxZombieLevel = listaLevels.ElementAt (0).enemies;
		} 

		else 
		{
			Debug.Log ("No puedo acceder al archivo");
		}
	}
}
