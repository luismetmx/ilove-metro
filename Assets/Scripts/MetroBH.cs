﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This Script handle a basic animation of the subway
/// </summary>

public class MetroBH : MonoBehaviour
{
    public static bool metroArrive;
    float speed;
    Animator anim;
	// Use this for initialization
	void Start ()
    {
        speed = -1.0f;
        metroArrive = false;
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(transform.position.y > 0)
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
        }

        else
        {
            speed = 0;
            anim.SetBool("isOpen", true);
            metroArrive = true;
        }
	}
}
