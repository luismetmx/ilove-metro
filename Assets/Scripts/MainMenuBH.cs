﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// This Script handle the MainMenu Scene (UI interactions, and unlock only the firts level to start)
/// </summary>

public class MainMenuBH : MonoBehaviour
{
    GameManagerBH gameManager;
	SaveLoadData datos;
	PlayerData playerInfo;

    void Awake()
    {
        gameManager = GameManagerBH.Instance;
    }

    void Start()
    {
        gameManager.OnStateChange += HandleOnStateChange;
		datos = new SaveLoadData ();
		playerInfo = datos.Load ();
		int[] levelsInfo = new int[] { 1, 0, 0, 0, 0, 0, 0, 0 };

		if (playerInfo == null) 
		{
			datos.Save (levelsInfo);
		}
    }

    public void OnPlay()
    {
        SceneManager.LoadScene("Levels");
    }

    public void OnRankings()
    {
    }

    public void OnSound()
    {
    }

    public void OnLogros()
    {

    }

    public void OnTutorial()
    {

    }

    public void HandleOnStateChange()
    {
        Debug.Log("Estoy en el estado: " + gameManager.gameState);
    }
}
