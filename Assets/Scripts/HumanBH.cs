﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

/// <summary>
/// This Script handle the Human characters behavior on the gameplay, loads the configurations of the Levels.xml file
/// movement, number of the humans in the level and the speed of the movement.
/// </summary>


public class HumanBH : MonoBehaviour
{
    float speed;
    RaycastHit2D touchEnemy;
	public TextAsset xmlLevelConfig;

    // Use this for initialization
    void Start()
    {
		LoadData();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));

//        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
//        {
//            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint((Input.GetTouch(0).position)), Vector2.zero);
//			if (hit != null && hit.collider != null) 
//			{
//				Debug.Log ("I'm hitting Human: "+hit.collider.name);
//				//Destroy (gameObject);
//			}
//        }

		if (Input.touchCount == 1)
		{
			Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			Vector2 touchPos = new Vector2(wp.x, wp.y);
			if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
			{
				GamePlayBH.lives--;
				Destroy (gameObject);
			}
		}
    }

	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Limite") 
		{
			Destroy (gameObject);
		}
	}

	void LoadData()
	{
		List<LevelConfig> listaLevels = new List<LevelConfig> ();

		if (xmlLevelConfig != null) 
		{

			XDocument xDoc = XDocument.Parse(xmlLevelConfig.text);

			listaLevels = (from item in xDoc.Descendants ("level")
				where item.Element ("id").Value.ToString() == LevelsBH.level.ToString()
				select new LevelConfig () 
				{
					id = int.Parse(item.Element ("id").Value),
					velocidad = float.Parse(item.Element("velocidad").Value)
				}).ToList ();

			speed = listaLevels.ElementAt (0).velocidad;
		} 

		else 
		{
			Debug.Log ("No puedo acceder al archivo");
		}
	}
}
