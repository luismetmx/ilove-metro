﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

/// <summary>
/// This class creates a file to save the progress of the player on a encrypted file
/// </summary>

public class SaveLoadData  
{
	PlayerData datos;

	void Awake()
	{
		// Forces a different code path in the BinaryFormatter that doesn't rely on run-time code generation (which would break on iOS).
		Environment.SetEnvironmentVariable ("MONO_REFLECTION_SERIALIZER", "yes");
	}

	public void Save(int []levelStatus) 
	{
		try 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (Application.persistentDataPath + "/iLoveMetro.dat");
			datos = new PlayerData ();
			datos.levelsStatus = levelStatus;
			bf.Serialize (file, datos);
			file.Close ();
			Debug.Log ("Datos de usuario guardados");
		}
		catch(Exception e)
		{
			Debug.Log("Error al guardar datos: " + e.Message);
		}
	}

	public void Save(PlayerData data) 
	{
		try 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (Application.persistentDataPath + "/iLoveMetro.dat");
			bf.Serialize (file, data);
			file.Close ();
			Debug.Log ("Datos de usuario guardados");
		}
		catch(Exception e)
		{
			Debug.Log("Error al guardar datos: " + e.Message);
		}
	}


	public PlayerData Load() 
	{
		try
		{

			if (File.Exists (Application.persistentDataPath + "/iLoveMetro.dat")) 
			{
				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Open (Application.persistentDataPath + "/iLoveMetro.dat", FileMode.Open);
				datos = (PlayerData)bf.Deserialize (file);
				file.Close ();
			}
			Debug.Log ("Datos de Usuario Cargados");
		}

		catch(Exception e)
		{
			Debug.Log("Error al cargar Datos: " + e.Message);
		}
		return datos;
	}
}
