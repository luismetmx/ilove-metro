﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// This script handle the behavior of the splash screen
/// </summary>

public class SplashScreenBH : MonoBehaviour
{
    float time;
    float timeMax;

    GameManagerBH gameManager;

    void Awake()
    {
        gameManager = GameManagerBH.Instance;
    }

    void Start()
    {
        gameManager.OnStateChange += HandleOnStateChange;
        timeMax = 5.0f;
        gameManager.SetGameState(GameState.SplashScreen);
    }

    void Update()
    {
        time += Time.deltaTime;

        if (time > timeMax)
        {
            SceneManager.LoadScene("MainMenu");
            gameManager.SetGameState(GameState.MainMenu);
        }
    }

    public void HandleOnStateChange()
    {
        Debug.Log("Estoy en el estado: " + gameManager.gameState);
    }
}
