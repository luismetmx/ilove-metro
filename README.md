This a simple Game made with Unity.

The objetive of the game is to stop the zombies (smash them) before they arrived to the subway (it's inspired on the Mexico City subway).

If you smash a Human Character you lose a live, if your lose all your lives the game ends.


Project Structure

-GameManagerBH
  *Uses the singleton pattern to handle the game state changes.
-SoundManagerBH
  *Uses the singleton pattern to handle the sound on the game.

Every scene has a script to control the UI.

This game uses a xml file to set the configurations of each level like:
*Max number of enemies on the scene.
*Max number of humans on the scene.
*Speed of movement of each character.

And I use a encrypted text file to save the player progress.

Comments at luis.sandoval.escobedo@hotmail.com